import axios from 'axios';

const service = axios.create({
    timeout: 10000,
    headers: {
        common: {
            Accept: 'application/json',
        },
        'Content-Type': 'application/json',
    },
});



const defaultBaseUrl = '/api';

export const api = (baseUrl = defaultBaseUrl, todo) => ({
    getAllTodos: () =>
        service.get(`${baseUrl}/todos`),

    createNewTodo: () =>
        service.post(`${baseUrl}/todos`, todo)
    /* other endpoints here */
});