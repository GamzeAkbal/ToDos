module.exports = {
  name: "todos",
  verbose: true,
  preset: '@vue/cli-plugin-unit-jest',
  transform: {
    '^.+\\.vue$': 'vue-jest'
  },

  //To run pact tests
  testEnvironment: "node",
  testMatch: ["**/*.test.(ts|js)", "**/*.it.(ts|js)","**/*.pact.spec.(ts|js)", '**/unit/**/*.spec.{j,t}s?(x)' ],
  watchPathIgnorePatterns: ["pact/logs/*", "pact/pacts/*"],



  //To run unit test
  //testMatch: ['**/unit/**/*.spec.{j,t}s?(x)'],
  //testEnvironment: 'jsdom',
}
