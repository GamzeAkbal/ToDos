# todoss

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Publish Pack to broker
```
pact-broker publish --consumer-app-version 1.0.0 --broker-base-url https://dius.pact.dius.com.au --broker-token SomeToken /path/to/pacts/consumer-provider.json --tag master
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
