describe("Creating ToDo ", () => {
    it("Display ToDo in the ToDoList", () =>{
        cy.visit("/")
        cy.get("[data-test='todoText']").type("Buy some milk")
        cy.get("[data-test='saveTodo']").click()
        cy.get("[data-test='todoText']").should("have.value", "")
        cy.contains("Buy some milk")
    })
})