const {Matchers} = require('@pact-foundation/pact')

const getAllTodosRequest = () => (
    {
        method: 'GET',
        path: '/todos',
        headers: {
            Accept: 'application/json',
        },
    })

const expectedGetAllTodosResponse = {
    status: 200,
    headers: {
        'Content-Type': 'application/json; charset=UTF-8'
    },
    body: Matchers.eachLike(
            {
                id: 0,
                text: '',
                isDone: false
            })

};

const createTodoRequest = () => (
    {
        method: 'POST',
        path: '/todos',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: {
            id: Matchers.like(0),
            text: Matchers.like('text'),
            isDone: Matchers.like(false)
        }

})

const expectedCreateNewTodoResponse = {
    status: 201,
    headers: {
        'Content-Type': 'application/json; charset=UTF-8',
    },
    body:  Matchers.like(
            {
                id: 0,
                text: 'Buy some milk',
                isDone: false
            },
        ),
}

module.exports = {
    couldGetAllTodos: () =>({
        state: 'i could get all todos',
        uponReceiving: 'Request for getting all todos',
        withRequest: getAllTodosRequest(),
        willRespondWith: expectedGetAllTodosResponse,
    }),

    couldCreateNewTodos: () =>({
        state: 'i could create new todo',
        uponReceiving: 'Request for create new todo',
        withRequest: createTodoRequest(),
        willRespondWith: expectedCreateNewTodoResponse,
    }),

}
