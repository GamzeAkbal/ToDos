const {pactWith} = require("jest-pact")
const interactions = require('./Todo.pact')
import {api} from "../../src/client/todo";

/**
 * @jest-environment node
 */
pactWith({
        consumer: 'MyConsumer',
        provider: 'MyProvider',
    },
    provider => {
        let client;
        const todo = {
            id: 0,
            text: "Buy some milk",
            isDone: false
        }
        beforeEach(() => {
            client = api(provider.mockService.baseUrl, todo)
        });
        describe('todo pact test', () => {
            test('Returned get all todos OK', async () => {
                await provider.addInteraction(interactions.couldGetAllTodos())
                const res = await client.getAllTodos()
                expect(res).toBeTruthy()
            })
            test('Returned create new todo OK', async () => {
                await provider.addInteraction(interactions.couldCreateNewTodos())
                const res = await client.createNewTodo()
                expect(res).toBeTruthy()
            })

            test('Returned right object when create new todo', async () => {
                await provider.addInteraction(interactions.couldCreateNewTodos())
                const res = await client.createNewTodo()
                expect(JSON.stringify(res.data)).toEqual(JSON.stringify(todo))
            })


        })
    }
)