import { mount } from "@vue/test-utils";
import CreateTodo from "@/components/CreateTodo.vue";

describe("Creating Todo", () => {
    let wrapper;

    beforeEach(()=>{
        wrapper = mount(CreateTodo)
    })
    describe("Todo form elements", () =>{
        it("Todo text input rendered", () =>{
            const inputElement = wrapper.find("[data-test='todoText']")
            expect(inputElement.exists()).toBeTruthy()
        })

    })
    describe("Clicking the save button", () => {

        beforeEach(async ()=>{
            wrapper.find("[data-test='todoText']").setValue("Buy some milk")
            wrapper.find("[data-test='saveTodo']").trigger("click")

        })

        it("Clears the text field", () =>{
            const inputElement = wrapper.find("[data-test='todoText']")
            expect(inputElement.element.value).toEqual("")
        })

        it("Emit the {saveTodo} event", () =>{
            let expectedObj = {
                id: 0,
                text: "Buy some milk",
                isDone: false
            }
            expect(wrapper.emitted("saveTodo")[0]).toEqual([expectedObj])
        })

    })
})